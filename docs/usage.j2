<h1>Usage</h1>

If you want to skip the documentation and jump straight to the <A HREF="https://bitbucket.org/laserllama/classforge/src/master/test/main.py">tests</A>, that's ok.
These docs are mostly an elaboration on the test code.

<!-------------------------------------------------------------------------------------------------------------->

{{ table_of_contents() }}

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Basic Example") }}

All ClassForge classes look like this:

{{ begin_code(language='python') }}from classforge import Class, Field

class Demo(Class):

    x = Field(default=3.14)
    # ...
{{ end_code() }}

There are just two simple rules:

<ul>
<li>Classes are subclasses of <i>Class</i></li>
<li>Data members inherit from (or are) declared with <i>Field</i>
</ul>

Access is as you would expect, despite lots of magic behind the scenes:

{{ begin_code(language='python') }}    obj = Demo(y=-2)
    assert obj.y == -2
    obj.y = 5
    assert obj.y == 5
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Typed Fields") }}

You can control what types objects are allowed to be set to. If you attempt to assign the wrong type, a <i>ValueError</i>
will be raised. ValueError is already a common python exception, so code using ClassForge will not have to be coded
differently:

{{ begin_code(language='python') }}class TypesDemo(Class):

    x = Field(type=int)
    y = Field(type=str)
    z = Field(type=SomeBasicClass)
{{ end_code() }}

If "type" isn't provided, each data member can take any value, just like python's default behavior.  We would
always recommend adding a type, however, because it's great documentation.

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Protecting Against Typos") }}

<p>
A common problem with python objects out of the box is that it is easy to typo a variable name
on assignment.  This problem is eliminated by using __SLOTS__, but __SLOTS__ introduces more
code to maintain. In ClassForge, this just works.
</p>

<p>
ClassForge classes never allow setting variables that are not
in the list of fields, which can help prevent typos in an otherwise
dynamic language.
</p>

{{ begin_code(language='python') }}class JunkInputDemo(Class):

    x = Field(type=int, default=15)

def test_junk_input():

    obj1 = JunkInputDemo()
    obj1.x = 5
    obj1.z = 24 # raises AttributeError
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Required Fields") }}

{{ begin_code(language='python') }}class RequiredDemo(Class):

    a = Field(type=int, default=42)
    b = Field(type=int, required=True)

obj1 = RequiredDemo(a=1, b=2)
obj2 = RequiredDemo(b=2)
obj3 = RequiredDemo() # Raises AttributeError

{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Multiple Choice Fields") }}

Sometimes you want to limit a field to a set of specific values. This is done as follows:

{{ begin_code(language='python') }}PETS = [ 'dog', 'cat', 'chicken' ]

class ChoicesDemo(Class):
    pet = Field(type=str, choices=PETS)

def test_choices():

    c = ChoicesDemo()
    c.pet = 'dog'
    c.pet = 'cat'
    c.pet = 'chicken'
    c.pet = 'velociraptor' # raises ValueError

{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Non-Nullable Fields") }}

It's arguably said that "null" or "None" is an anti-pattern.  You can protect against objects being set to None as follows:

{{ begin_code(language='python') }}class NullableDemo(Class):

    a = Field(type=int, nullable=False, default=0)
    b = Field(type=int, nullable=False)
{{ end_code() }}

If a default is not set for a nullable field, the value will still be 'None' until it is set either by a constructor call
or a later assignment, but the value 'None' cannot be assigned to the data member.

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Immutable Fields") }}

Immutable systems and functional programming are popular at the moment, and rightfully so.  Class Forge can optionally
help you keep values from being reassigned once they are set:

{{ begin_code(language='python') }}class MutableDemo(Class):

    a = Field(type=int)
    b = Field(type=int, default=10, mutable=False)

# ...
obj = MutableDemo(a=5, b=2)
{{ end_code() }}

In the above example, a and b cannot be reassigned, and an attempt to do so raises an error.

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Init Hooks") }}

<p>
A common problem among Python developers subclassing any object is knowing how to call "super()" from within a constructor.
Should it be called before or after meaningful logic?  What parameters does the parent class need?
Worse, it's easy to forget to call it altogether.
</p>

<p>ClassForge makes this easy by providing an alternative function that you can use instead of '__init__()' called 'on_init()'.
'on_init' methods in parent classes are called automatically, without any need to guess about the right use of 'super'.  The result
is less boilerplate code and much less chance of making a mistake.
</p>

{{ begin_code(language='python') }}class ProductLessThanOneHundred(Class):
    a = Field(type=int, default=10)
    b = Field(type=int, default=10)

    def on_init(self):
        assert (self.a * self.b) < 100
{{ end_code() }}

Note that 'on_init' takes only the 'self' parameter.

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Custom Accessors") }}

Custom accessors allow you to remap a value after retrieving it.

Favoring 'convention over configuration', the system will look for a <i>get_fieldname</i> method for
any field without having to declare it. You can rename this if you like.

{{ begin_code(language='python') }}class AccessorDemo(Class):

    a = Field(type=int, accessor='renamed_get_a')
    b = Field(type=int)

    def renamed_get_a(self, value):
        return value * 10

    def get_b(self, actual_value):
        return actual_value * 100
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Custom Mutators") }}

Custom mutators are similar to custom accessors, but are called before a value is stored into the object system.
Code could choose to raise <i>ValueError</i> to reject a value.

{{ begin_code(language='python') }}class MutatorDemo(Class):

    a = Field(type=int, mutator='renamed_set_a')
    b = Field(type=int)
    c = Field(type=int)

    def renamed_set_a(self, input_value):
        return input_value * 10

    def set_b(self, input_value):
        return input_value * 100

    def set_c(self, input_value):
        if input_value < 5000:
            raise ValueError("too low")
        return input_value
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Basic Object Serialization") }}

ClassForge allows objects - even nested objects to be dumped to dictionaries as well as loaded
from them.  For version 0.1, this only works if the objects are basic types or a subclass of <i>Class</i>.

{{ begin_code(language='python') }}class SerializationDemo(Class):

    f = Field(type=int, default=10)
    g = Field(type=int, default=20)
    h = Field(type=int, default=30)

obj = SerializationDemo(f=5)

serialized = obj.to_dict()
assert serialized['f'] == 5
assert serialized['g'] == 20

obj2 = SerializationDemo.from_dict(serialized)
assert obj2.f == 5
assert obj2.g == 20
assert obj2.h == 30
{{ end_code() }}

Additionally there are JSON and YAML variants.

{{ begin_code(language='python') }}
json_str = obj.to_json(sort_keys=True, indent=4)
obj = SerializationDemo.from_json(json_str)

yaml_str = obj.to_yaml()
obj = SerializationDemo.from_yaml(yaml_str)
{{ end_code() }}

These methods pass any unrecognized arguments to the json and PyYAML libraries, which is why 'sort_keys' and 'indent'
are available in the examples above.

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Nested Object Serialization") }}

Class objects in ClassForge can include other objects, allowing for automatic recursive serialization and deserialization.

{{ begin_code() }}
class NestedSerializationDemo(Class):

    c = Field(type=SerializationDemo)
    d = Field(type=int, default=40)
    e = Field(type=int, default=50)

class DeeperSerializationDemo(Class):

    a = Field(type=NestedSerializationDemo)
    b = Field(type=int, default=60)


obj1 = DeeperSerializationDemo(
   a = NestedSerializationDemo(
       c = SerializationDemo(
           g  = 80
       ),
       d = 90
   ),
   b = 70,
)

data = obj1.to_dict()
assert data == {'a': {'c': {'f': 10, 'g': 80, 'h': 30}, 'd': 90, 'e': 50}, 'b': 70}

obj2 = DeeperSerializationDemo.from_dict(data)
assert obj2.b == 70
assert obj2.a.c.f == 10
{{ end_code() }}

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Remapped Object Serialization") }}

Sometimes an API you have to work with isn't necessarily pythonic, or has other API inconsistencies.  "Remap" features
in Classforge allow serialization and deserialization using variants of the field names available in the ClassForge
objects. A primary example of this is a good python class will use under_scores for variable names, but some JSON
APIs may use camelCase or EvenSomethingINCONSISTENT. By defining these alternative representations in the Field definition,
code documentation is improved, and the need for remapping code to trickle throughout an implementation is avoided.

{{ begin_code(language='python') }}ACME_CLOUD='acme_cloud'
STARK_INDUSTRIES='stark_industries'

class AdvancedSerializationDemo(Class):

    basic_example    = Field(type=int, remap='basicExample')

    hidden           = Field(type=int, hidden=True)

    count            = Field(type=int, remap=dict(acme_cloud='productAmount'))
    instance_size    = Field(type=int, remap=dict(acme_cloud='instanceSize', stark_industries='instanceCapacity'))
    sometimes_hidden = Field(type=int, remap=dict(acme_cloud=None))

obj1 = AdvancedSerializationDemo(basic_example=0, count=1, instance_size=2,sometimes_hidden=4)
data = obj1.to_dict()
assert data == dict(basicExample=0, count=1, instance_size=2, sometimes_hidden=4)

acme_data = obj1.to_dict(ACME_CLOUD)
assert acme_data == dict(basicExample=0, productAmount=1, instanceSize=2)

stark_data = obj1.to_dict(STARK_INDUSTRIES)
assert stark_data == dict(basicExample=0, count=1, instanceCapacity=2, sometimes_hidden=4)

acme_obj = AdvancedSerializationDemo.from_dict(acme_data, ACME_CLOUD)
stark_obj = AdvancedSerializationDemo.from_dict(stark_data, STARK_INDUSTRIES)
{{ end_code() }}

<p>
The conversions are bidirectional. Note that there are multiple ways to express remap behavior.  In the above example,
'count' is only remapped for the 'acme_cloud' remap flavor, while instance_size is remapped in two different ways. The
field 'sometimes_hidden' is included in the 'stark_industries' serialization, but is not present in the 'acme_cloud' dict.
</p>

<p>
This solution isn't always going to be 100% of what is needed for API translation between disparate systems,
but can help make things easier.
</p>

<!--------------------------------------------------------------------------------------------------------------->

{{ section("Inheritance") }}

<p>
Inheritance in ClassForge also works as you would expect.
</p>

<p>
The example below is somewhat involved by design, as it attempts to show
various overrides and shadowings of types at various layers of a diamond
inheritance structure.
</p>

<p>
You might have been warned about diamond inheritance in a CS class, but we
felt it was important to show it works.
</p>

{{ begin_code(language='python') }}class DiamondBase(Class):

    a = Field(type=int, default=1)
    b = Field(type=int, default=2)
    c = Field(type=int, default=3)
    d = Field(type=int, default=4)
    e = Field(type=int, default=5)
    f = Field(type=int, default=6)
    g = Field(type=str, default="Hi world")

class LeftSide(DiamondBase):

    e = Field(type=int, default=7)
    f = Field(type=int, default=8)

    def __init__(self, **kwargs):
        # make sure constructors don't mess anything up
        super(LeftSide, self).__init__(**kwargs)

class RightSide(DiamondBase):

    e = Field(type=int, default=9)
    f = Field(type=int, default=10)

class Child(LeftSide, RightSide):

    b = Field(type=int, default=12)
    c = Field(type=str, default='hello')
    f = Field(type=int, default=11)

obj4 = Child()
assert obj4.b == 12
assert obj4.c == 'hello'
assert obj4.f == 11
assert obj4.e in [ 7, 9 ] # heh
assert obj4.a == 1
{{ end_code() }}

<!---------------------------------------------------------------------------------------------------------->

{{ section("Subclassing Field") }}

<p>
Methods in the <i>Field</i> class starting with <i>field_</i> may be easily subclassed, to create custom field types.
</p>

{{ begin_code(language='python') }}class Base64DemoField(Field):

    def __init__(self, **kwargs):
        kwargs['type'] = str
        super(Base64DemoField, self).__init__(**kwargs)

    def field_access(self, value):
        # changes the value before accessing
        return value

    def field_mutate(self, value):
        # changes the value before storing
        return value

    def field_encode(self, value, remap_name=None):
        return base64.b64encode(bytes(value, 'utf-8'))

    def field_decode(self, value, remap_name=None):
        return str(base64.b64decode(value), 'ascii')
{{ end_code() }}

<!---------------------------------------------------------------------------------------------------------->

{{ section("Strict Classes") }}

Classes derived from StrictClass instead of Strict have the following rules:

<ul>
<li>mutable is False by default</li>
<li>if a default is not provided (the original default is None), the field is assumed required</li>
<li>the type field is always required</li>
</ul>

{{ begin_code(language='python') }}class StrictDemo(StrictClass):

   x = Field(type=int, default=25)

obj = StrictDemo(x=5)

obj.x = 10 # error
obj = StrictDemo() # error
{{ end_code() }}


<!---------------------------------------------------------------------------------------------------------->

{{ section("Future Features") }}

<p>
ClassForge is young, and new features will continue to be added quickly. Included subclasses of fields are at the top of our list,
as well as serialization improvements. If you have other ideas, we'd be glad to hear them.  Stop by {{ doc('contributing') }} for contact information.
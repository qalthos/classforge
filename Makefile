html:
	(cd docs; make)

tests:
	PYTHONPATH=. pytest --cov=classforge test/main.py --capture=fd -x

clean:
	-(rm dist/*)

wheel: clean
	python setup.py sdist bdist_wheel

pypi_upload: wheel
	# project owners only, after bumping version
	twine upload dist/*.tar.gz
